import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  categories: any;
  
  constructor(
    private http: HttpClient
  ) {}

  getHeader() {
    return new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json');
  }

  async getCategories() {
    await this.http.get("https://api.publicapis.org/categories", { headers: this.getHeader() }).toPromise().then((res: any) => {
      console.log(res);
      this.categories = res;
    }).catch((error) => {
      console.log(error);
    });
  }

}
