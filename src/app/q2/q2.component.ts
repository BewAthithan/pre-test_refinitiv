import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/service/service.service';

@Component({
  selector: 'app-q2',
  templateUrl: './q2.component.html',
  styleUrls: ['./q2.component.scss']
})
export class Q2Component implements OnInit {

  categories: any;

  constructor(
    private service: ServiceService
  ) {}

  ngOnInit(): void {
    this.getCategories();
  }

  async getCategories () {
    await this.service.getCategories();
    this.categories = this.service.categories;
  }

  searchText(event: any) {
    console.log(event.value);
    let categories = new Array();
    let val = event.value.toLowerCase();
    if (val && val.trim() != "") {
      for(var i = 0; i < this.categories.length; i++) {
        if(this.categories[i].toLowerCase().includes(val) == true) {
          categories.push(this.categories[i]);
        }      
      }
      this.categories = categories;
    } else {
      this.getCategories();
    }
  }

}
