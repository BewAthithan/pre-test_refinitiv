import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-q1',
  templateUrl: './q1.component.html',
  styleUrls: ['./q1.component.scss']
})
export class Q1Component implements OnInit {

  number = new FormControl();
  selectVal = "isPrime";
  isCalculate: any

  constructor() {}

  ngOnInit(): void {}

  getNumber() {
    if(!Number.isInteger(this.number.value)) {
      this.number.patchValue(Math.round(this.number.value));
    } else if(this.number.value < 0) {
      this.number.patchValue(1);
    }
    console.log(this.number.value);
    this.selectCal({value: this.selectVal});
  }

  selectCal(event: any) {
    if(event.value == "isPrime") {
      this.selectVal = "isPrime";
      let isPrime = true;
      if (this.number.value == 1) {
        isPrime = false;
      } else if (this.number.value > 1) {
        for (let i = 2; i < this.number.value; i++) {
          if (this.number.value % i == 0) {
            isPrime = false;
            break;
          }
        }
        if (isPrime) {
          this.isCalculate = true;
        } else {
          this.isCalculate = false;
        }
      }
    } else if(event.value == "isFibonacci") {
      this.selectVal = "isFibonacci";
      this.isCalculate = this.isFibonacci(this.number.value);
    }
  }

  isFibonacci(n: any){
    return this.isPerfectSquare(5 * n * n + 4) || this.isPerfectSquare(5 * n * n - 4);
  }

  isPerfectSquare(x: any){
    let s = Math.sqrt(x);
    return (s * s == x);
  }

}
